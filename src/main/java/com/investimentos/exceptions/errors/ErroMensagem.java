package com.investimentos.exceptions.errors;

import java.util.HashMap;

public class ErroMensagem {

    private String erro;
    private String mensagemErro;

    private HashMap<String, ErroObjeto> camposErro;

    public String getErro() {
        return erro;
    }

    public void setErro(String erro) {
        this.erro = erro;
    }

    public String getMensagemErro() {
        return mensagemErro;
    }

    public void setMensagemErro(String mensagemErro) {
        this.mensagemErro = mensagemErro;
    }

    public HashMap<String, ErroObjeto> getCamposErro() {
        return camposErro;
    }

    public void setCamposErro(HashMap<String, ErroObjeto> camposErro) {
        this.camposErro = camposErro;
    }

    public ErroMensagem() {
    }

    public ErroMensagem(String erro, String mensagemErro, HashMap<String, ErroObjeto> camposErro) {
        this.erro = erro;
        this.mensagemErro = mensagemErro;
        this.camposErro = camposErro;
    }
}
