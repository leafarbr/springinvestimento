package com.investimentos.exceptions.errors;

public class ErroObjeto {

    String mensagemErro;
    String valorRejeitado;

    public String getMensagemErro() {
        return mensagemErro;
    }

    public void setMensagemErro(String mensagemErro) {
        this.mensagemErro = mensagemErro;
    }

    public String getValorRejeitado() {
        return valorRejeitado;
    }

    public void setValorRejeitado(String valorRejeitado) {
        this.valorRejeitado = valorRejeitado;

    }

    public ErroObjeto() {
    }

    public ErroObjeto(String mensagemErro, String valorRejeitado) {
        this.mensagemErro = mensagemErro;
        this.valorRejeitado = valorRejeitado;
    }
}
