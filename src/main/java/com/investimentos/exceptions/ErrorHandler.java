package com.investimentos.exceptions;

import com.investimentos.exceptions.errors.ErroMensagem;
import com.investimentos.exceptions.errors.ErroObjeto;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.lang.invoke.MethodHandle;
import java.util.HashMap;
import java.util.List;

@ControllerAdvice
public class ErrorHandler {

    private String mensagemPadrao;

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(code = HttpStatus.UNPROCESSABLE_ENTITY)
    @ResponseBody
    public ErroMensagem manipulacaoDeErrosDeValidacao(MethodArgumentNotValidException exception)
    {

        HashMap<String, ErroObjeto> erros = new HashMap<>();
        BindingResult resultado = exception.getBindingResult();

        List<FieldError> fieldErrors = resultado.getFieldErrors();

        for(FieldError erro : fieldErrors) {
            erros.put(erro.getField(), new ErroObjeto(erro.getDefaultMessage(), erro.getRejectedValue().toString()));

        }

        ErroMensagem mensagemDeErro = new ErroMensagem(HttpStatus.UNPROCESSABLE_ENTITY.toString(),
                mensagemPadrao, erros);

        return mensagemDeErro;
    }

}
