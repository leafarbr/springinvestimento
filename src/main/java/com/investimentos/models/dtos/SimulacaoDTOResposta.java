package com.investimentos.models.dtos;

public class SimulacaoDTOResposta
{
  private double rendimentoPorMes;
  private double montante;

    public double getRendimentoPorMes() {
        return rendimentoPorMes;
    }

    public void setRendimentoPorMes(double rendimentoPorMes) {
        this.rendimentoPorMes = rendimentoPorMes;
    }

    public double getMontante() {
        return montante;
    }

    public void setMontante(double montante) {
        this.montante = montante;
    }

    public SimulacaoDTOResposta() {
    }
}
