package com.investimentos.repositories;

import com.investimentos.models.Produto;
import org.springframework.data.repository.CrudRepository;

public interface ProdutoRespository extends CrudRepository<Produto, Integer> {

}
