package com.investimentos.controllers;

import com.investimentos.models.Produto;
import com.investimentos.models.Simulacao;
import com.investimentos.models.dtos.SimulacaoDTOResposta;
import com.investimentos.services.ProdutoService;
import com.investimentos.services.SimulacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/investimentos")
public class ProdutoController {

    @Autowired
    private ProdutoService produtoService;

    @Autowired
    private SimulacaoService simulacaoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Produto incluirProduto(@RequestBody Produto produto)
    {
        return produtoService.incluirProduto((produto));

    }

    @PostMapping("{idproduto}/simulacao")
    @ResponseStatus(HttpStatus.CREATED)
    public SimulacaoDTOResposta simulaInvestimento(@PathVariable(name = "idproduto") int idproduto, @RequestBody Simulacao simulacao){

        simulacao.setIdproduto(idproduto);
        try {
            return simulacaoService.incluirSimulacao(simulacao);
        }
        catch (RuntimeException exception)
        {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @GetMapping
    public Iterable<Produto> listarProdutos()
    {
        return produtoService.listarProdutos();
    }
}
