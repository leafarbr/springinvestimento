package com.investimentos.services;

import com.investimentos.models.Produto;
import com.investimentos.models.Simulacao;
import com.investimentos.models.dtos.SimulacaoDTOResposta;
import com.investimentos.repositories.ProdutoRespository;
import com.investimentos.repositories.SimulacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class SimulacaoService {

    @Autowired
    private SimulacaoRepository simulacaoRepository;

    @Autowired
    private ProdutoRespository produtoRespository;

    public Iterable<Simulacao> listarSimulacoes() {
        Iterable<Simulacao> objetoSimulacoes = simulacaoRepository.findAll();
        return objetoSimulacoes;
    }

    public SimulacaoDTOResposta incluirSimulacao(Simulacao simulacao)
    {

        Optional<Produto> optProduto = produtoRespository.findById(simulacao.getIdproduto());

        if (optProduto.isPresent())
        {
            Produto objProduto = optProduto.get();
            Simulacao objsimulacao = simulacaoRepository.save(simulacao);

            SimulacaoDTOResposta objSimulacaoDTO = new SimulacaoDTOResposta();

            double rendimentoMensal = calcularRendimentoMensal(objsimulacao.getValorAplicado(),
                                                               objProduto.getRendimentoAoMes());

            double montante = calcularMontante(objsimulacao.getValorAplicado(),objProduto.getRendimentoAoMes(),
                                               objsimulacao.getQuantidadeMeses());

            objSimulacaoDTO.setRendimentoPorMes(rendimentoMensal);
            objSimulacaoDTO.setMontante(montante);

            return objSimulacaoDTO;
        }

        throw new RuntimeException("Produto de investimento não encontrado");
    }



    private double calcularRendimentoMensal(double valor, double taxa)
    {
        return valor * taxa;
    }

    private double calcularMontante(double valorpresente, double taxa, int prazo)
    {
        double rendimentomensal;

       for (int i = 0; i < prazo; i++)
       {
               rendimentomensal = calcularRendimentoMensal(valorpresente,taxa);
               valorpresente = valorpresente + rendimentomensal;
       }

       return valorpresente;
    }

}
