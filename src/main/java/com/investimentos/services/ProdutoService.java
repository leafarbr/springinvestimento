package com.investimentos.services;

import com.investimentos.models.Produto;
import com.investimentos.repositories.ProdutoRespository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ProdutoService {

    @Autowired
    private ProdutoRespository produtoRepository;

    public Produto incluirProduto(Produto produto) {
        Produto objectProduto = produtoRepository.save(produto);
        return objectProduto;
    }

    public Iterable<Produto> listarProdutos()
    {
        Iterable<Produto> listaProdutos = produtoRepository.findAll();
        return listaProdutos;
    }

    public Produto buscarPorId(int idProduto)
    {
        Optional<Produto> optionalProduto =  produtoRepository.findById(idProduto);
        if (optionalProduto.isPresent()) {
            return optionalProduto.get();
        }
        else {
            throw new RuntimeException("O Produto de investimento não foi encontrado");
        }
    }

}
